<!DOCTYPE html>
<html>
<head>
	<title>Shanawut C.</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="Description" content="">
	<meta name="keywords" content="">

	<!-- <link rel='icon' href='image/favicon.png' sizes="32x32"> -->
	<!-- FONT -->
	<link href="https://fonts.googleapis.com/css?family=Kanit:200,300,400,500" rel="stylesheet">
	<!-- <link rel="stylesheet" media="screen" href="libra/bootstrap/bootstrap.css"> -->
	<link rel="stylesheet" media="screen" href="libra/fontawesome/css/font-awesome.min.css">

	<!-- <link rel="stylesheet" media="screen" href="css/sivan.css"> -->
	<link rel="stylesheet" media="screen" href="css/style.css">
	<!-- <link rel="stylesheet" media="screen" href="css/style_index.css"> -->

	<script src="js/jquery-2.1.3.min.js"></script>
	<!-- <script src="libra/bootstrap/bootstrap.min.js"></script> -->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

	<script src="js/jquery-1.11.3.min.js"></script>
	<![endif]-->

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MGHZMZ8');
	</script>
	<!-- End Google Tag Manager -->
</head>
<body>
	<!-- Google Tag Manager -->
	<script>
	dataLayer = [{
		'pageCategory': 'signup',
		'visitorType': 'high-value'
	}];
	</script>
	<!-- Google Tag Manager (noscript) -->
	<noscript>
	<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MGHZMZ8" height="0" width="0" style="display:none;visibility:hidden"></iframe>
	</noscript>
	<!-- End Google Tag Manager (noscript) -->

	<section id='scene1' onclick="Uncover(this)">
		<div class="maincover">
			<div class="maincontainer mainshade textmasked">
				<div class="bolding right"><div class="mainshade"></div></div>

				<span>S</span>
				<span>I</span>
				<span>V</span>
				<span>A</span>
				<span>N</span>

				<div class="bolding"><div class="mainshade"></div></div>
			</div>
			<div class="welcometext">view my portfolio <span class="fa fa-caret-right"></span></div>
		</div>
		<div class="uncover"></div>
	</section>
	<section id="scene2">
		<div class="intro-content layer" data-depth='0.10' >
			<h3 class="mainshade textmasked" onclick="TagMng()">
				INTRODUCING
			</h3>
			<div class="text">
				<p>
					I'm Sivan, a Senior Interactive Designer with over three years experience working in both a corporate and freelance capacity. mainly focusing on designing and building websites/web applications which are simple to use and look great.
				</p>
				<p>
					I really happy to be part of many digital creation. So please don't hesitate to get in touch. and feel free to enjoy my portfolio.
				</p>
			</div>
		</div>
		<div class="line-set layer" data-depth='0.05'>
			<div class="line-a"></div>
			<div class="line-b"></div>
		</div>
		<div id="particles_01"></div>
	</section>
	<section id="scene3">
		<p>
			PORTFOLIO
		</p>
		<div class="layer" data-depth='0.15'>
			<div class="bg">
				<div class="displayer">
					<div class="projectexample">
						<div></div>
						<section></section>
					</div>
				</div>
			</div>
		</div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</section>
	<section id="scene4">
		<div class="layer" data-depth='0.1'>
			<div class="bg">
				<div class="displayer">
					<div class="projectexam">
						<iframe id="webdisplay" src="http://crehand.com" seamless="seamless"></iframe>
					</div>
				</div>
			</div>
		</div>
		<div class="headline layer" data-depth='0.2'>
			<h2>WORK</h2>
			<div class="worklists">
				<div class="worklist">1</div>
			</div>
		</div>
		<div class="workname layer" data-depth='0.2'>
			<h3></h3>
		</div>
		<div id="lineset4" class="line-set layer pose-1" data-depth='0.05'>
			<div class="line-a"></div>
			<div class="line-b"></div>
		</div>
	</section>
</body>
</html>

<script src="libra/parallax/parallax.js"></script>
<script src="libra/particles/particles.min.js"></script>


<script type="text/javascript">
	function Uncover(abc){

		console.log('ok');
		$(abc).addClass('active');
		$(window).scrollTop(0);
	}
	var changewebstage = 1;
	var changewebhttp = '';
	var changewebname = ["",
	"XXX", 
	"Treetop Adventurepark", 
	"Silvetive",
	"Tokyo Disney Sea",
	"PENN Holding",
	"Penndemy",
	"Crehand",
	];
	var changewebarray = ["",
	"http://silvetive.com/_7thstreet", 
	"https://www.treetopadventurepark.com/", 
	"http://silvetive.com",
	"http://silvetive.com/_disney.html",
	"http://silvetive.com/_penn",
	"http://silvetive.com/_penndemy",
	"http://www.crehand.com",
	"http://silvetive.com/_kenkone",
	];
	var changewebcolor = ["",
	"#0F8541", 
	"orange", 
	"#da3163",
	"#00B3F8",
	"#274B83",
	"#002B41",
	"#4D2F75",
	"#78685A"
	];
	function ChangeWeb(page){
		if (page != null) {
			changewebstage = page;
		}
		changewebhttp = changewebarray[changewebstage];
		if (changewebhttp == null) {
			changewebstage = 1;
			changewebhttp = changewebarray[changewebstage];
		}
		$('#scene4').css('background-color',changewebcolor[changewebstage]);
		$('#lineset4').attr('data-pose',changewebstage);
		$('.worklist').removeClass('active');
		$(".worklist[data-work='"+changewebstage+"']").addClass('active');
		$('.workname').html("<h3>"+changewebname[changewebstage]+"</h3>");

		console.log(changewebstage+" : "+changewebhttp);
		changewebstage++;
		$('.projectexam').html("<iframe id='webdisplay' src='"+changewebhttp+"' seamless='seamless'></iframe>")
	}
	function WorkList(){
		var worklisthtml = '';
		for (var i = 1; i < changewebarray.length; i++) {
			// console.log(i);
			worklisthtml += "<div class='worklist' data-work='"+i+"' onclick='ChangeWeb("+i+")'>"+i+"</div>"
		}
		$('.worklists').html(worklisthtml);
	}
</script>
<script type="text/javascript">
	$(document).ready(function(){
		WorkList();
		ChangeWeb();
		// var scene = document.getElementById('scene');
		var scene = document.getElementById('scene2');
		var parallax = new Parallax(scene);
		var scene3 = document.getElementById('scene3');
		var parallax3 = new Parallax(scene3);
		var scene4 = document.getElementById('scene4');
		var parallax4 = new Parallax(scene4);

		particlesJS("particles_01", {
			"particles":{
				"number":{
					"value":6,"density":{
						"enable":true,
						"value_area":800
					}
				},
				"color":{

					"value":"#16CCC8"
					// "value":"#00ffc3"
					// "value":"#ff0096"
				},
				"shape":{
					"type":"polygon",
					"stroke":{
						"width":0,"color":"#000"
					},
					"polygon":{
						"nb_sides":6
					},
					"image":{
						"src":"img/github.svg",
						"width":100,
						"height":100
					}
				},
				"opacity":{
					"value":0.02,
					"random":true,
					"anim":{
						"enable":false,
						"speed":1,
						"opacity_min":0.1,
						"sync":false
					}
				},
				"size":{
					"value":160,
					"random":false,
					"anim":{
						"enable":true,
						"speed":10,
						"size_min":40,
						"sync":false
					}
				},
				"line_linked":{
					"enable":false,
					"distance":200,
					"color":"#ffffff",
					"opacity":1,
					"width":2
				},
				"move":{
					"enable":true,
					"speed":3,
					"direction":"none",
					"random":false,
					"straight":false,
					"out_mode":"out",
					"bounce":false,
					"attract":{
						"enable":false,
						"rotateX":600,
						"rotateY":1200
					}
				}
			},
			"interactivity":{
				"detect_on":"canvas",
				"events":{
					"onhover":{
						"enable":false,
						"mode":"grab"
					},
					"onclick":{
						"enable":false,
						"mode":"push"
					},
					"resize":true
				},
				"modes":{
					"grab":{
						"distance":400,
						"line_linked":{
							"opacity":1
						}
					},
					"bubble":{
						"distance":400,
						"size":40,
						"duration":2,
						"opacity":8,
						"speed":3
					},
					"repulse":{
						"distance":200,
						"duration":0.4
					},
					"push":{
						"particles_nb":4
					},
					"remove":{
						"particles_nb":2
					}
				}
			},
			"retina_detect":true
		});
		var count_particles, stats, update;
		// stats = new Stats;
		// stats.setMode(0);
		// stats.domElement.style.position = 'absolute';
		// stats.domElement.style.left = '0px';
		// stats.domElement.style.top = '0px';
		// document.body.appendChild(stats.domElement);
		// count_particles = document.querySelector('.js-count-particles');
		// update = function() { stats.begin();
			// stats.end();
			// if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) { count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
			// } requestAnimationFrame(update);
		// };
		// requestAnimationFrame(update);;
	})
	
	function TagMng(){
		dataLayer.push({
			'color': 'red',
			'conversionValue': 50,
			'event': 'dl_Login',
			'dl_Login': 'fire'
		});
	}
</script>